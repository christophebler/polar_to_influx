import xmltodict
from influxdb import InfluxDBClient
import time
import argparse


# python PolarUploader.py --path /home/christoph/Dokumente/20180526171949_0.tcx -host 127.0.0.1 --database ebler --user ebler -pw ebler -v


# argumente einlesn
parser = argparse.ArgumentParser()
parser.add_argument("-p","--path", help="Path to TCX File",type=str)
parser.add_argument("-host","--hostaddress", help="database host",type=str)
parser.add_argument("-db","--database", help="database name ()",type=str)
parser.add_argument("-user","--username", help="database username ()",type=str)
parser.add_argument("-pw","--password", help="database password",type=str)
parser.add_argument("-v","--verbose",help = "turn output on",action="store_true")
args = parser.parse_args()

# verbindung zur db aufbauen
#client = InfluxDBClient(127.0.0.1, 8086, 'ebler', 'ebler', 'ebler') #host port user password db name
client = InfluxDBClient(args.hostaddress, 8086, args.username, args.password,args.database) #host port user password db name

#xml in dicht zerlegen
with open(args.path) as fd:
    doc = xmltodict.parse(fd.read())
    # fuer Distanzberechnung
    olddist = 0
    # Alle Trackpoint elemente vereinzlen
    for tp in  doc['TrainingCenterDatabase']['Activities']['Activity']['Lap']['Track']['Trackpoint']:
# JSON body zusammensetzen
        json_body = [
                {
                    "measurement": "training",

                    "time": tp['Time'],
                    "fields": {
                        "DistanceMeters":float(tp['DistanceMeters']),
                        "HeartRateBpm":float( tp['HeartRateBpm']['Value']),
                        "Cadence": float(tp['Cadence']),
                        "speed": (float(tp['DistanceMeters']) - olddist)*3.6,

                    }
                }
            ]
        olddist = float(tp['DistanceMeters'])

        #daten in Datenbank schreiben
        client.write_points(json_body)

        #Debug ausgabe
        if args.verbose:
            print json_body
